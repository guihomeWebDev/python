def multi_callback(a, b):
    return a*b

def add_callback(a, b):
    return a+b

def display_table(n, operator, operation_callback):
    for i in range(1, 10+1):
        print(i, operator, n, "=", operation_callback(n, i))

display_table(2, "x", multi_callback)
display_table(2, "+", add_callback)

#function lambda
