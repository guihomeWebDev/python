

def sum(*number):
    """
    Sum all the numbers in the list
    """
    result = 0
    for n in number:
        result += n
    return result

print(sum(10, 15, 1, 22, 56))