

def sum(*number):
    """
    Sum all the numbers in the list
    """
    result = 0
    for n in number:
        result += n
    return result

print(sum(1, 2, 3, 4, 5))