# nom = "guihome"
# age = 44

# print("bonjour", nom, "tu as", age, "ans")

def display_info(nom = "", age = 0):
    if nom == "":
        print("La personne n'a pas de nom")
    else:
        if age == 0:
            print("La personne s'appelle", nom)
        else:
            print("La personne s'appelle", nom, "et a", age, "ans")
        print(" votre prénom comporte", len(nom), "caractères")


display_info(age= 44)